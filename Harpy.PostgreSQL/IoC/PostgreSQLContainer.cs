﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;
using System;
using System.Data;

namespace Harpy.PostgreSQL.IoC {

    public static class PostgreSQLContainer {

        public static IDbConnection UsePostgreSQLConnection( this DbContextOptionsBuilder dbContextOptions, string connectionString = null, Action<NpgsqlDbContextOptionsBuilder> postgreSQLOptionsAction = null ) {
            if ( string.IsNullOrEmpty( connectionString ) )
                connectionString = "Data Source=:memory:";

            var dbConnection = new NpgsqlConnection( connectionString );
            dbConnection.Open( );

            dbContextOptions.UseNpgsql( dbConnection, postgreSQLOptionsAction );

            return dbConnection;
        }

        public static NpgsqlDbContextOptionsBuilder AddMigrations( this NpgsqlDbContextOptionsBuilder optionsBuilder, string migrationsAssembly = "" ) {
            return optionsBuilder
                .MigrationsAssembly( migrationsAssembly )
                .MigrationsHistoryTable( "migrations_history" );
        }
    }
}
