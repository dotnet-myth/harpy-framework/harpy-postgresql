﻿using Hangfire.PostgreSql;
using System;

namespace Harpy.PostgreSQL.Presentation.Extensions {

    public static class HangfirePostgreSQL {

        public static PostgreSqlStorage UseStorage( string hangfireConnection  ) {
            if ( string.IsNullOrEmpty( hangfireConnection ) )
                throw new ArgumentNullException( hangfireConnection, "Connection string is not valid!" );

            var options = new PostgreSqlStorageOptions {
            };

            return new PostgreSqlStorage( hangfireConnection, options );
        }
    }
}
